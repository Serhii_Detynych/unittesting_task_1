package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorPowTest extends BaseCalculatorTest {

    @ParameterizedTest
    @MethodSource("powDataTestProvider")
    public void powTest(double base, double exponent, double expectedResult) {
        assertEquals(expectedResult,calculator.pow(base,exponent), 0.0001);
    }

    public static Stream<Arguments> powDataTestProvider() {
        return Stream.of(
                arguments(2.0,3.0,8.0),
                arguments(-9.0,-3.0,-0.0013),
                arguments(-3.0, 3.0, -27),
                arguments(6,-5,0.0001),
                arguments(-9.0,0,1.0),
                arguments(9.0,0,1.0),
                arguments(2.7,3.6,35.7198),
                arguments(-2.3,9.6,NaN),
                arguments(2.3,-9.6,3.3683),
                arguments(4, POSITIVE_INFINITY,POSITIVE_INFINITY),
                arguments(-4, NEGATIVE_INFINITY,0.0),
                arguments(5, NaN, NaN),
                arguments(NaN,4,NaN)
        );
    }
}
