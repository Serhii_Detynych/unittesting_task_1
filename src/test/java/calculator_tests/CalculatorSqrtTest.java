package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorSqrtTest extends BaseCalculatorTest {

    @ParameterizedTest
    @MethodSource("sqrtTestDataProvider")
    public void sqrtTest(double value, double expectedResult) {
        assertEquals(expectedResult, calculator.sqrt(value), 0.0001);
    }

    public static Stream<Arguments> sqrtTestDataProvider() {
        return Stream.of(
                arguments(4, 2),
                arguments(7.6, 2.7568),
                arguments(-4, NaN),
                arguments(NaN, NaN),
                arguments(POSITIVE_INFINITY, POSITIVE_INFINITY),
                arguments(0.0, 0.0),
                arguments(4, 2)
        );
    }
}
