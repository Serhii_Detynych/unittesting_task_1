package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorCosTest extends BaseCalculatorTest {

    @ParameterizedTest
    @MethodSource("cosTestDataProvider")
    public void cosTest(double value, double expectedResult) {
        assertEquals(expectedResult, calculator.cos(value), 0.0001);
    }

    public static Stream<Arguments> cosTestDataProvider() {
        return Stream.of(
                arguments(0, 1.0),
                arguments(9, -0.9111),
                arguments(-9, -0.9111),
                arguments(13.2, 0.8058),
                arguments(NEGATIVE_INFINITY, NaN),
                arguments(POSITIVE_INFINITY, NaN),
                arguments(NaN, NaN)
        );
    }
}
