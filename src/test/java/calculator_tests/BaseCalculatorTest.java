package calculator_tests;

import com.epam.tat.module4.Calculator;
import org.junit.jupiter.api.BeforeAll;

public class BaseCalculatorTest {

    protected static Calculator calculator;

    @BeforeAll
    public static void setUp(){
        calculator = new Calculator();
    }
}
