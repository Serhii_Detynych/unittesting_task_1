package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.NaN;
import static java.lang.Double.POSITIVE_INFINITY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorSinTest extends BaseCalculatorTest {

    @ParameterizedTest
    @MethodSource("sinTestDataProvider")
    public void sinTest(double value, double expectedResult) {
        assertEquals(expectedResult, calculator.sin(value), 0.0001);
    }

    public static Stream<Arguments> sinTestDataProvider() {
        return Stream.of(
                arguments(0, 0),
                arguments(2.0, 0.9092),
                arguments(-2.0, -0.9092),
                arguments(2.8, 0.3349),
                arguments(-2.8, -0.3349),
                arguments(POSITIVE_INFINITY, NaN),
                arguments(NaN, NaN)
        );
    }
}
