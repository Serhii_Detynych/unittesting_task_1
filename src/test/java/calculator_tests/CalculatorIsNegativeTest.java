package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorIsNegativeTest extends BaseCalculatorTest {
    @ParameterizedTest
    @MethodSource("isNegativeTestDataProvider")
    public void isNegativeTest(long value, boolean expectedResult) {
        assertEquals(expectedResult, calculator.isNegative(value));
    }

    public static Stream<Arguments> isNegativeTestDataProvider() {
        return Stream.of(
                arguments(0, false),
                arguments(9, false),
                arguments(-9, true)
        );
    }
}
