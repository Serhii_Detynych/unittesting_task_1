package calculator_tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorMultTest extends BaseCalculatorTest {

    @ParameterizedTest
    @CsvFileSource(resources = "longMultResources.csv")
    @DisplayName("Test mult() with long parameters")
    public void longMultTest(long firstParam, long secondParam, long expectedResult) {
        assertEquals(expectedResult, calculator.mult(firstParam,secondParam));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "doubleMultResources.csv")
    @DisplayName("Test mult() with double parameters")
    public void doubleMultTest(double firstParam, double secondParam, double expectedResult) {
        assertEquals(expectedResult, calculator.mult(firstParam,secondParam));
    }

}
