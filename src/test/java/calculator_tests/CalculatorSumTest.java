package calculator_tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorSumTest extends BaseCalculatorTest {

    @ParameterizedTest
    @CsvFileSource(resources = "doubleSumResources.csv")
    @DisplayName("Test sum() with double parameters")
    public void doubleSumTest(double firstParam, double secondParam, double expectedResult) {
        assertEquals(expectedResult, calculator.sum(firstParam, secondParam), 0.001);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "longSumResources.csv")
    @DisplayName("Test sum() with long parameters")
    public void longSumTest(long firstParam, long secondParam, long expectedResult) {
        assertEquals(expectedResult, calculator.sum(firstParam, secondParam));
    }
}
