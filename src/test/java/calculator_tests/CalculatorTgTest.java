package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorTgTest extends BaseCalculatorTest {
    @ParameterizedTest
    @MethodSource("tgTestDataProvider")
    public void tgTest(double value, double expectedResult) {
        assertEquals(expectedResult, calculator.tg(value), 0.0001);
    }

    public static Stream<Arguments> tgTestDataProvider() {
        return Stream.of(
                arguments(7.0, 0.8714),
                arguments(-7.0, -0.8714),
                arguments(0.0, 0.0),
                arguments(POSITIVE_INFINITY, NaN),
                arguments(NEGATIVE_INFINITY, NaN),
                arguments(NaN, NaN)

        );
    }


}
