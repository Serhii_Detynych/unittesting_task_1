package calculator_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class CalculatorSubTest extends BaseCalculatorTest {

    @ParameterizedTest
    @CsvFileSource(resources = "doubleSubResources.csv")
    @DisplayName("Test sub() with double parameters")
    public void doubleSubTest(double firstParam, double secondParam, double expectedResult) {
        Assertions.assertEquals(expectedResult, calculator.sub(firstParam, secondParam));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "longSubResources.csv")
    @DisplayName("Test sub() with long parameters")
    public void longSubTest(long firstParam, long secondParam, long expectedResult) {
        Assertions.assertEquals(expectedResult, calculator.sub(firstParam, secondParam));
    }
}
