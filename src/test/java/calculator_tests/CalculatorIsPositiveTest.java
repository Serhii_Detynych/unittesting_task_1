package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorIsPositiveTest extends BaseCalculatorTest {
    @ParameterizedTest
    @MethodSource("isPositiveTestDataProvider")
    public void isPositiveTest(long value, boolean expectedResult) {
        assertEquals(expectedResult, calculator.isPositive(value));
    }

    public static Stream<Arguments> isPositiveTestDataProvider() {
        return Stream.of(
                arguments(0, false),
                arguments(9, true),
                arguments(-9, false)
        );
    }
}
