package calculator_tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorDivTest extends BaseCalculatorTest {

    @Test
    @DisplayName("Divide by zero with long parameters")
    public void divideByZeroWithLongParametersTest() {
        Exception exception = Assertions.assertThrows(NumberFormatException.class, () -> calculator.div(5, 0));
        String expectedMessage = "Attempt to divide by zero";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    @DisplayName("Divide by zero with double parameters")
    public void divideByZeroWithDoubleParametersTest() {
        Exception exception = Assertions.assertThrows(NumberFormatException.class, () -> calculator.div(5.0, 0.0));
        String expectedMessage = "Attempt to divide by zero";
        String actualMessage = exception.getMessage();
        assertEquals(expectedMessage, actualMessage);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "longDivResources.csv")
    public void divideWithValidLongParametersTest(long firstParam, long secondParam, long expectedResult) {
        assertEquals(expectedResult, calculator.div(firstParam,secondParam));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "doubleDivResources.csv")
    public void divideWithValidDoubleParametersTest(double firstParam, double secondParam, double expectedResult) {
        assertEquals(expectedResult, calculator.div(firstParam,secondParam));
    }


}
