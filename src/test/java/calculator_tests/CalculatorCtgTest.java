package calculator_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static java.lang.Double.*;
import static java.lang.Double.NaN;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class CalculatorCtgTest extends BaseCalculatorTest {
    @ParameterizedTest
    @MethodSource("ctgTestDataProvider")
    public void ctgTest(double value, double expectedResult) {
        assertEquals(expectedResult, calculator.ctg(value), 0.0001);
    }

    public static Stream<Arguments> ctgTestDataProvider() {
        return Stream.of(
                arguments(0, POSITIVE_INFINITY),
                arguments(9, -2.2108),
                arguments(-9, 2.2108),
                arguments(13.2, 1.3611),
                arguments(NEGATIVE_INFINITY, NaN),
                arguments(POSITIVE_INFINITY, NaN),
                arguments(NaN, NaN)
        );
    }
}
