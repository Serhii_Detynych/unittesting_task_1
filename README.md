# UT_T1

## HOME TASK

As a basis for this home task, use Calculator-1.0.jar

### Write unit tests for the calculator jar library (Calculator-1.0.jar)

- Add Calculator-1.0.jar to your project reference;
- Write tests using any testing framework (JUnit or TestNG);
- Each class should test only one method of the library;
- Tests pre-conditions and post-conditions should be implemented with appropriate annotations;
- Run tests in IDE:
- For TestNG only: create .xml configuration file and use it to run tests. Create at least one
  test group. Some tests can depend on this group, or this group can be used to
  exclude/include tests from launch using XML-suites;
- Create parametrized tests (data-driven approach);
- Configure and execute tests in parallel mode;


## ACCEPTANCE CRITERIA

- A separate Unit Test class exists for each method of Calculator-1.0.iar;
- JUnit or TestNG is used in home task:
- Tests have pre-conditions and post-conditions;
- Screenshots with results of running in IDE are available; 
- For TestNG only: Xml configuration exists for suite. This configuration allows use/run group of tests;
- Data-driven approach is demonstrated with test framework features;
- Parallel execution of tests is demonstrated with test framework features;


## BONUS TASK 

- Calculator-1.0.jar contains couple defects. Each found and reported defect gives you +0,1 as a bonus